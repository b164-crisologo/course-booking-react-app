import { Navigate} from 'react-router-dom';
import {useContext, useEffect} from 'react'
//Redirect
import UserContext from '../userContext'


export default function Logout() {

	const { unsetUser, setUser} = useContext(UserContext);

	//Clear the localStorage
	unsetUser()

	//By adding the Use
	useEffect(() => {
		//Set the user state back into it's original value
		setUser({id: null})
	}, [])

	return(

		<Navigate to="/" />

		)


}