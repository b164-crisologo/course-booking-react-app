import { useState, useEffect } from 'react';
import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom'

export default function CourseCard({courseProp}) {

	//Use the state hook for this component to be able to store its state
	/*
		Syntax:
			const [getter, setter] = useState(initalGetterValue)
	*/
	// const [count, setCount] = useState(0);
	// const [seats, setSeats] = useState(30)

	// function enroll() {
	// 	setCount(count + 1)
	// 	setSeats(seats - 1)
	// 	console.log('Enrollees: ' + count)

	// }

	// useEffect(() => {
	// 	if(seats === 0) {
	// 		alert("No more seats available!");
	// 	}

	// }, [seats]);


	//console.log(props.courseProp)
	const {name, description, price, _id} = courseProp

	return(

			<Card>
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>Php {price}</Card.Text>
					{/*<Card.Text>Enrollees: {count}</Card.Text>
					<Card.Text>Seats: {seats}</Card.Text>*/}
					<Button variant="primary" as={Link} to={`/courses/${_id}`}>Details</Button>
				</Card.Body>
			</Card>
		)

}